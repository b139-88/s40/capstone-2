
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String
	},
	imageUrl: {
		type: String
	},
	categories: {
		type: Array
	},
	properties: {
		type: Array
	},
    price: {
        type: Number,
        required: [true, "Price name is required"]
    },
	quantity: {
        type: Number,
        default: 0
    },
	isPublished: {
		type: Boolean,
		default: false
	}
},
{
    timestamps: true
});


module.exports = mongoose.model("Product", productSchema);
