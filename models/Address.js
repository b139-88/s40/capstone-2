const mongoose = require("mongoose");

const addressSchema = new mongoose.Schema({
    userId: {
		type: String,
		required: [true, "User ID is required"],
        ref: "User"
	},
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    addressLine1: {
        type: String,
        required: [true, "Address line 1 is required"]
    },
    addressLine2: {
        type: String
    },
    city: {
        type: String,
        required: [true, "City is required"]
    },
    state: {
        type: String,
        required: [true, "State is required"]
    },
    zipCode: {
        type: String,
        required: [true, "Zip Code is required"]
    },
    mobileNumber: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    isDefaultBilling: {
        type: Boolean,
        default: false
    },
    isDefaultShipping: {
        type: Boolean,
        default: false
    },
    addressType: {
        type: String,
        required: [true, "Address type is required"]
    },
},
{
    timestamps: true
});


    module.exports = mongoose.model("Address", addressSchema);
    