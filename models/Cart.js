
const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"],
        ref: "User"
	},
    products: [{
		productId: {
            type: String,
            required: [true, "Product ID is required"],
            ref: 'Product'
        },
        quantity: {
            type: Number,
            default: 1
        },
        cartLineItemTotal: {
            type: Number
        }
	}],
    cartTotal: {
        type: Number
    }
},
{
    timestamps: true
});


module.exports = mongoose.model("Cart", cartSchema);
