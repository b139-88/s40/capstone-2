
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "Product name is required"],
        ref: "User"
	},
	products: [{
		productId: {
            type: String,
            ref: 'Product'
        },
        quantity: {
            type: Number,
            default: 1
        },
        cartLineItemTotal: {
            type: Number
        }
	}],
	billingAddress: {
		type: Object,
        required: [true, "Address is required"],
        ref: 'Address'
	},
	shippingAddress: {
		type: Object,
        required: [true, "Address is required"],
        ref: 'Address'
	},
    itemsSubTotal: {
        type: Number,
        required: [true, "Items subtotal is required"]
    },
    shippingCost: {
        type: Number,
        required: [true, "Shipping cost is required"]
    },
    orderTotal: {
        type: Number,
        required: [true, "Order Total is required"]
    },
    paymentMethod: {
		type: String,
		required: [true, "Payment method is required"]
	},
    shippingMethod: {
		type: String,
		required: [true, "Shipping method is required"]
	},
    status: {
        type: String,
        default: "Pending"
    }
},
{
    timestamps: true
});


module.exports = mongoose.model("Order", orderSchema);
