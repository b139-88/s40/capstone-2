const router = require('express').Router();   
const productControllers = require("../controllers/productControllers");
const { ensureAuthorized } = require('../lib/utils');

router.get('/', (req, res) => {
    productControllers.getAllProducts().then(result => res.status(result.status).send(result.payload));
});

router.post('/', ensureAuthorized, (req, res, next) => {
    productControllers.createProduct(req).then(result => res.status(result.status).send(result.payload));

    // let result = productControllers.createProduct(req);
    // console.log(result);
    // res.status(result.status).send(result.payload);
});

router.get('/:productId', (req, res) => {
    productControllers.getProduct(req).then(result => res.status(result.status).send(result.payload));
});

router.put('/:productId', ensureAuthorized, (req, res, next) => {
    productControllers.updateProduct(req).then(result => res.status(result.status).send(result.payload));
});

router.patch('/:productId/archive', ensureAuthorized, (req, res, next) => {
    productControllers.archiveProduct(req).then(result => res.status(result.status).send(result.payload));
});

router.patch('/:productId/unarchive', ensureAuthorized, (req, res, next) => {
    productControllers.unarchiveProduct(req).then(result => res.status(result.status).send(result.payload));
});

module.exports = router;