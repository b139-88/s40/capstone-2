const router = require('express').Router();   
const cartControllers = require("../controllers/cartControllers");
const { ensureAuthorized } = require('../lib/utils');

router.get('/', ensureAuthorized, (req, res, next) => {
    cartControllers.getCart(req).then(result => res.status(result.status).send(result.payload));
});

router.post('/', ensureAuthorized, (req, res, next) => {
    cartControllers.addToCart(req).then(result => res.status(result.status).send(result.payload));
});


module.exports = router;