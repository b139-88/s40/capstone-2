const router = require('express').Router();   
const orderControllers = require("../controllers/orderControllers");
const { ensureAuthorized } = require('../lib/utils');

router.get('/', ensureAuthorized, (req, res, next) => {
    orderControllers.getAllOrders(req).then(result => res.status(result.status).send(result.payload));
});

router.get('/:orderId', ensureAuthorized, (req, res, next) => {
    orderControllers.getOrder(req).then(result => res.status(result.status).send(result.payload));
});


module.exports = router;