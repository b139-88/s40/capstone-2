const router = require('express').Router();   
const userControllers = require("../controllers/userControllers");
const { ensureAuthorized } = require('../lib/utils');

router.post('/register', (req, res) => {
  userControllers.register(req.body).then(result => res.status(result.status).send(result.payload));
});

router.post('/login', (req, res, next) => {
  userControllers.login(req.body).then(result => res.status(result.status).send(result.payload));
});

router.get('/details', ensureAuthorized, (req, res, next) => {
  userControllers.details(req).then(result => res.status(result.status).send(result.payload));
});

router.post('/address', ensureAuthorized, (req, res, next) => {
  userControllers.addAddress(req).then(result => res.status(result.status).send(result.payload));
});

router.get('/address', ensureAuthorized, (req, res, next) => {
    userControllers.getAddresses(req).then(result => res.status(result.status).send(result.payload));
});

router.get('/address/:id', ensureAuthorized, (req, res, next) => {
    userControllers.getAddress(req).then(result => res.status(result.status).send(result.payload));
});

router.delete('/address/:id', ensureAuthorized, (req, res, next) => {
    userControllers.deleteAddress(req).then(result => res.status(result.status).send(result.payload));
});

router.put('/address/:id', ensureAuthorized, (req, res, next) => {
    userControllers.updateAddress(req).then(result => res.status(result.status).send(result.payload));
});

router.patch('/:id/admin', ensureAuthorized, (req, res) => {
  userControllers.setAsAdmin(req).then(result => res.status(result.status).send(result.payload));
  //let result = userControllers.setAsAdmin(req);
  //res.status(result.status).send(result.payload);
});

router.patch('/:id/password', ensureAuthorized, (req, res) => {
  userControllers.changePassword(req).then(result => res.status(result.status).send(result.payload));
});

router.post('/checkout', ensureAuthorized, (req, res, next) => {
  userControllers.checkout(req).then(result => res.status(result.status).send(result.payload));
});

router.get('/orders', ensureAuthorized, (req, res, next) => {
  userControllers.getUserOrders(req).then(result => res.status(result.status).send(result.payload));
});

router.get('/orders/:orderId', ensureAuthorized, (req, res, next) => {
    userControllers.getUserOrder(req).then(result => res.status(result.status).send(result.payload));
});

router.get('/protected', ensureAuthorized, (req, res) => {
  res.status(200).send(`You are worthy!`);
});


module.exports = router;