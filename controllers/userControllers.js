const mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const User = mongoose.model('User');
const Order = mongoose.model('Order');
const Address = mongoose.model('Address');
const Cart = mongoose.model('Cart');
const Product = mongoose.model('Product');
const utils = require('../lib/utils');

module.exports.register = (req) => {
    let newUser = new User({
        firstName: req.firstName,
        lastName: req.lastName,
        email: req.email,
        password: bcrypt.hashSync(req.password, 10),
    });

	return newUser.save()
        .then(user => {

            let jwt = utils.issueJWT(user);

            return {
                status: 201,
                payload: {
                    success: true,
                    message: `User successfully registered.`,
                    token: jwt.token,
                    expiresIn: jwt.expires
                }
            };
        })
        .catch(error => {
            console.error(error);
            return {
                status: 409,
                payload: {
                    success: false,
                    message: `Email already exists.`
                }
            };
        }
    );
};

module.exports.login = (req) => {
    return User.findOne({ email: req.email })
        .then(user => {
            console.log(user);
            if(!user) {
                return {
                    status: 401,
                    payload: {
                        success: false,
                        message: `User does not exists.`
                    }
                };
            }

            let isPasswordCorrect = bcrypt.compareSync(req.password, user.password);

            if (isPasswordCorrect) {
                let jwt = utils.issueJWT(user);

                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `User successfully logged in.`,
                        token: jwt.token,
                        expiresIn: jwt.expires
                    }
                };
            } else {
                return {
                    status: 401,
                    payload: {
                        success: false,
                        message: `Incorrect password. Please try again.`
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
        }
    );
};

module.exports.details = (req) => {
    return User.findById(req.user._id)
        .then(user => {
            console.log(req.user)
            if(!user) {
                return {
                    status: 401,
                    payload: {
                        success: false,
                        message: `User does not exists.`
                    }
                };
            }

            user.password = '';
            return {
                status: 200,
                payload: {
                    success: true,
                    message: `User successfully logged in.`,
                    user: user
                }
            };
        })
        .catch(error => {
            console.error(error);
        }
    );
};

module.exports.addAddress = (req, res) => {
    let address = req.body;

    address.userId = req.user._id;

    let newAddress = new Address(address);

    return newAddress.save()
        .then(address => {
            return {
                status: 201,
                payload: {
                    success: true,
                    message: `Address successfully added.`,
                    address: address
                }
            };
        })
        .catch(error => {
            console.error(error);
            return {
                status: 500,
                payload: {
                    success: false,
                    message: `An error has occurred. Please try again.`
                }
            };
        }
    );
};

module.exports.getAddresses = (req, res) => {
    return Address.find({ userId: req.user._id })
    .then(addresses => {
        return {
            status: 200,
            payload: {
                success: true,
                message: `Addresses successfully retrieved.`,
                addresses: addresses
            }
        };
    }).catch(error => {
        console.error(error);
        return {
            status: 500,
            payload: {
                success: false,
                message: `Ann error occurred while trying to retrieve your addresses. Please contact support if issue persists.`,
            }
        };
    });
}

module.exports.getAddress = (req, res) => {
    return Address.findById(req.params.id)
    .then(address => {
        return {
            status: 200,
            payload: {
                success: true,
                message: `Addresses successfully retrieved.`,
                address: address
            }
        };
    }).catch(error => {
        console.error(error);
        return {
            status: 500,
            payload: {
                success: false,
                message: `Ann error occurred while trying to retrieve this address. Please contact support if issue persists.`,
            }
        };
    });
}

module.exports.deleteAddress = (req, res) => {
    return Address.findByIdAndDelete(req.params.id)
    .then(address => {
        return {
            status: 200,
            payload: {
                success: true,
                message: `Addresses successfully deleted.`
            }
        };
    }).catch(error => {
        console.error(error);
        return {
            status: 500,
            payload: {
                success: false,
                message: `Ann error occurred while trying to delete this address. Please contact support if issue persists.`,
            }
        };
    });
}

module.exports.updateAddress = (req, res) => {
    return Address.findByIdAndUpdate(req.params.id, req.body)
    .then(address => {
        return {
            status: 200,
            payload: {
                success: true,
                message: `Addresses successfully updated.`
            }
        };
    }).catch(error => {
        console.error(error);
        return {
            status: 500,
            payload: {
                success: false,
                message: `Ann error occurred while trying to delete this address. Please contact support if issue persists.`,
            }
        };
    });
}

module.exports.changePassword = (req) => {
    return User.findById(req.params.id)
        .then(user => {
            if(!user) {
                return {
                    status: 401,
                    payload: {
                        success: false,
                        message: `User does not exists.`
                    }
                };
            }

            if(req.body.newPassword != req.body.newConfirmPassword) {
                return {
                    status: 406,
                    payload: {
                        success: false,
                        message: `Password confirmation does not match. Please try again.`
                    }
                };
            }

            let isPasswordCorrect = bcrypt.compareSync(req.body.currentPassword, user.password);

            if (isPasswordCorrect) {
                let update = {
                    password: bcrypt.hashSync(req.body.newPassword, 10)
                };

                return User.findByIdAndUpdate(req.params.id, update)
                    .then(user => {
                        return {
                            status: 200,
                            payload: {
                                success: true,
                                message: `Password successfully changed.`
                            }
                        };
                    });
            } else {
                return {
                    status: 401,
                    payload: {
                        success: false,
                        message: `Incorrect current password. Please try again.`
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: false,
                    message: `An error has occurred. Please try again.`
                }
            };
        }
    );
};

module.exports.setAsAdmin = (req) => {
    if (!req.user.isAdmin) {
        return new Promise((resolve) => {
            resolve({
                status: 401,
                payload: {
                    success: false,
                    message: `You are not authorized to perform this action.`
                }
            });
        });
    }

    let update = {
        isAdmin: true
    };

    return User.findByIdAndUpdate(req.params.id, update)
        .then(user => {
            if (user) {
                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `User successfully set as administrator.`
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 500,
                payload: {
                    success: false,
                    message: `An error has occurred. Please try again.`
                }
            };
        }
    );
};

module.exports.getUserOrders = (req) => {
    return Order.find({ userId: req.user._id })
    .populate(['userId', 'products.productId', 'billingAddress', 'shippingAddress'])
    .then(order => {
        return {
            status: 200,
            payload: {
                success: true,
                message: `Orders successfully retrieved.`,
                orders: order
            }
        };
    }).catch(error => {
        console.error(error);
        return {
            status: 500,
            payload: {
                success: false,
                message: `Ann error occurred while trying to retrieve your orders. Please contact support if issue persists.`,
            }
        };
    });
};

module.exports.getUserOrder = (req) => {
    return Order.findById(req.params.orderId)
    .populate(['userId', 'products.productId', 'billingAddress', 'shippingAddress'])
    .then(order => {
        return {
            status: 200,
            payload: {
                success: true,
                message: `Orders successfully retrieved.`,
                order: order
            }
        };
    }).catch(error => {
        console.error(error);
        return {
            status: 500,
            payload: {
                success: false,
                message: `Ann error occurred while trying to retrieve your orders. Please contact support if issue persists.`,
            }
        };
    });
};

module.exports.checkout = async (req) => {
    let cart = await Cart.findOne({ userId: req.user._id})
        .then(cart => cart);

    //Check each item in cart if in stock and not greater than current stocks
    let stockCheckResult = await checkIfCartItemsAvailable(cart);

    if (!areAllInStock(stockCheckResult)) {
        return {
            status: 400,
            payload: {
                success: false,
                message: `An item in your cart is no longer available. Please check the items and try again.`,
            }
        };
    } else {
        let newOrder = new Order({
            userId: req.user.id,
            products: cart.products,
            billingAddress: req.body.billingAddress,
            shippingAddress: req.body.shippingAddress,
            itemsSubTotal: cart.cartTotal,
            shippingCost: req.body.shippingCost,
            orderTotal: cart.cartTotal + req.body.shippingCost,
            paymentMethod: req.body.paymentMethod,
            shippingMethod: req.body.shippingMethod
        });

        return newOrder.save()
            .then(() => {
                //Call function to deduct cart items to current inventory stock
                deductCartItemsQuantity(cart.products);

                //Delete cart of user
                console.log(cart._id);
                Cart.findByIdAndDelete(cart._id)
                    .then(result => result);

                return {
                    status: 201,
                    payload: {
                        success: true,
                        message: `Your order has been successfully submitted.`,
                    }
                };
            })
            .catch(error => {
                console.error(error);
                return {
                    status: 500,
                    payload: {
                        success: false,
                        message: `Ann error occurred while trying to process your order. Please contact support if issue persists.`,
                    }
                };
            }
        );
    }
};

const checkIfCartItemsAvailable = async (cart) => {
    return await Promise.all(
        cart.products.map(prod => {
            return Product.findById(prod.productId)
            .then(product => {
                if (prod.quantity <= product.quantity) {
                    return true;
                } else {
                    return false;
                }
            });
        })
    );
};

const deductCartItemsQuantity = (cart) => {
    cart.forEach(cartItem => {
        Product.findById(cartItem.productId)
            .then(product => {
                product.quantity -= cartItem.quantity;
                product.save();
            }
        );
    });
};

const areAllInStock = array => array.every(Boolean);