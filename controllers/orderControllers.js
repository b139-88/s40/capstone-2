const mongoose = require('mongoose');
const Order = mongoose.model('Order');

module.exports.getAllOrders = (req) => {
    if (!req.user.isAdmin) {
        return new Promise((resolve) => {
            resolve({
                status: 401,
                payload: {
                    success: false,
                    message: `You are not authorized to perform this action.`
                }
            });
        });
    }

    return Order.find()
        .populate(['userId', 'products.productId', 'billingAddress', 'shippingAddress'])
        .then(orders => {
            return {
                status: 200,
                payload: {
                    success: false,
                    message: `Successfully retrieved all orders.`,
                    orders: orders
                }
            };
        }
    );
};

module.exports.getOrder = (req) => {
    return Order.findById(req.params.orderId)
    .populate(['userId', 'products.productId', 'billingAddress', 'shippingAddress'])
    .then(order => {
        return {
            status: 200,
            payload: {
                success: true,
                message: `Orders successfully retrieved.`,
                order: order
            }
        };
    }).catch(error => {
        console.error(error);
        return {
            status: 500,
            payload: {
                success: false,
                message: `Ann error occurred while trying to retrieve your orders. Please contact support if issue persists.`,
            }
        };
    });
};