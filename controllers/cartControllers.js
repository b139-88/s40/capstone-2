const mongoose = require('mongoose');
const Cart = mongoose.model('Cart');
const Product = mongoose.model('Product');

module.exports.addToCart = (req) => {
    let reqCart = req.body;

    return Cart.findOne({ userId: req.user.id })
        .then(async cart => {
            if (cart) {
                let isProductAvailable = await isAvailableOrActive(reqCart.productId);

                if (!isProductAvailable) {
                    return {
                        status: 409,
                        payload: {
                            success: false,
                            message: `Item is out of stock or not published yet.`
                        }
                    };
                }

                let indexOfProduct = cart.products.findIndex(p => p.productId === reqCart.productId);

                if (indexOfProduct > -1) {
                    cart.products[indexOfProduct].quantity += reqCart.quantity;
                } else {
                    cart.products.push(reqCart);
                }

                await cart.save();

                totalCartOfUser(req.user.id);

                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `Item successfully added to cart.`
                    }
                };
            } else {
                let isProductAvailable = await isAvailableOrActive(reqCart.productId);

                if (!isProductAvailable) {
                    return {
                        status: 409,
                        payload: {
                            success: false,
                            message: `Item is either out of stock or not published yet.`
                        }
                    };
                }
                
                let newCart = new Cart({
                    userId: req.user.id,
                    products: [{
                        productId: reqCart.productId,
                        quantity: reqCart.quantity
                    }]
                });

                await newCart.save();

                totalCartOfUser(req.user.id);

                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `Item successfully added to cart.`
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: true,
                    message: `Something went wrong. Please try again later.`,
                }
            };
        }
    );
};

module.exports.getCart = (req) => {
    return Cart.findOne({ userId: req.user.id })
    .populate(['userId', 'products.productId'])
        .then(cart => {
            if (cart) {
                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `Successfully retrieved cart.`,
                        cart: cart
                    }
                };
            } else {
                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `User has no items in cart.`,
                        cart: cart
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: false,
                    message: `Something went wrong. Please try again later.`,
                }
            };
        });
};

isAvailableOrActive = (productId) => {
    return Product.findById(productId)
        .then(result => {
            if (result) {
                if (!result.isPublished || result.quantity < 1) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    );
};

totalCartOfUser = (userId) => {
    Cart.findOne({ userId: userId })
        .then(async cart => {
            let total = 0;

            for (let x = 0; x < cart.products.length; x++) {
                let price = await Product.findById(cart.products[x].productId).then(product => product.price);

                let lineItemTotal = price * cart.products[x].quantity;

                total += lineItemTotal;

                cart.products[x].cartLineItemTotal = lineItemTotal;
                cart.cartTotal = total;
            }

            cart.save();
        }
    )
    .catch(error => {
        console.error(error);
        return {
            status: 400,
            payload: {
                success: true,
                message: `Something went wrong. While trying to compute cart total. Please try again later.`,
            }
        };
    });
};