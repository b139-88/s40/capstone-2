const mongoose = require('mongoose');
const Product = mongoose.model('Product');

module.exports.createProduct = async (req) => {
    if (!req.user.isAdmin) {
        return {
            status: 401,
            payload: {
                success: false,
                message: `You are not authorized to create new products.`
            }
        };
    }

    let newProduct = new Product(req.body);

    return newProduct.save()
        .then(product => {
            if (product) {
                return {
                    status: 201,
                    payload: {
                        success: true,
                        message: `New product successfully created.`,
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: false,
                    message: `Something went wrong. Please try again later.`
                }
            };
        }
    );
};

module.exports.getAllProducts = () => {
    return Product.find({ isPublished: true })
        .then(products => {
            if (products) {
                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `All products successfully retrieved.`,
                        products: products
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: true,
                    message: `Something went wrong. Please try again later.`,
                }
            };
        }
    );
};

module.exports.getProduct = (req) => {
    return Product.findById(req.params.productId)
        .then(product => {
            if (product) {
                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `Product successfully retrieved.`,
                        product: product
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: true,
                    message: `Something went wrong. Please try again later.`,
                }
            };
        }
    );
};

module.exports.updateProduct = (req) => {
    if (!req.user.isAdmin) {
        return {
            status: 401,
            payload: {
                success: false,
                message: `You are not authorized to update products.`
            }
        };
    }

    return Product.findByIdAndUpdate(req.params.productId, req.body)
        .then(product => {
            if (product) {
                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `Product successfully updated.`
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: true,
                    message: `Something went wrong. Please try again later.`,
                }
            };
        }
    );
};

module.exports.archiveProduct = (req) => {
    if (!req.user.isAdmin) {
        return {
            status: 401,
            payload: {
                success: false,
                message: `You are not authorized to update products.`
            }
        };
    }

    return Product.findByIdAndUpdate(req.params.productId, { isPublished: false })
        .then(product => {
            if (product) {
                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `Product successfully archived.`
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: true,
                    message: `Something went wrong. Please try again later.`,
                }
            };
        }
    );
};

module.exports.unarchiveProduct = (req) => {
    if (!req.user.isAdmin) {
        return {
            status: 401,
            payload: {
                success: false,
                message: `You are not authorized to update products.`
            }
        };
    }

    return Product.findByIdAndUpdate(req.params.productId, { isPublished: true })
        .then(product => {
            if (product) {
                return {
                    status: 200,
                    payload: {
                        success: true,
                        message: `Product successfully unarchived.`
                    }
                };
            }
        })
        .catch(error => {
            console.error(error);
            return {
                status: 400,
                payload: {
                    success: true,
                    message: `Something went wrong. Please try again later.`,
                }
            };
        }
    );
};