const jsonwebtoken = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
const passport = require('passport');

const pathToKey = path.join(__dirname, '..', 'rsa_priv.pem');
const PRIV_KEY = fs.readFileSync(pathToKey, 'utf8');

require('dotenv').config();

/**
 * -------------- HELPER FUNCTIONS ----------------
 */

/**
 * @param {*} user - The user object.  We need this to set the JWT `sub` payload property to the MongoDB user ID
 */
module.exports.issueJWT = (user) => {
    const _id = user._id;

    const expiresIn = '4h';

    const payload = {
        sub: _id,
        iat: Math.floor(Date.now() / 1000),
        iss: 'RDC'
    };

    const signedToken = jsonwebtoken.sign(payload, PRIV_KEY, { expiresIn: expiresIn, algorithm: 'RS256' });

    return {
        token: signedToken,
        expires: expiresIn
    };
};

/**
 * Check if the token of the user is valid.
 * Return 401 if invalid token
 */
module.exports.ensureAuthorized = (req, res, next) => {
    passport.authenticate('jwt', { 
        session: false
    })(req, res, next);
};